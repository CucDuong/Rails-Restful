FactoryBot.define do
  factory :credentials do
    email { Faker::Internet.email }
    password { Faker::Internet.password }
  end
end