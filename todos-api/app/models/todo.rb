class Todo < ApplicationRecord
    # we have :destroy here to mention that the associated items of a todo will be deleted along with the destruction of the todo
    has_many :items, dependent: :destroy

    validates_presence_of :title, :created_by
end
