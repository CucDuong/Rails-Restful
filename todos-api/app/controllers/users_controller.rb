class UsersController < ApplicationController
  skip_before_action :authorize_request, only: :create

  def create
    # We use Active Record's `create!` method so that in the event there's an error,
    # an exception will be raised and handled in the exception handler
    user = User.create!(user_params)
    auth_token = AuthenticateUser.new(user.email, user.password).call
    response = { message: Message.account_created, auth_token: auth_token }
    json_response(response, :created)
  end

  private

  def user_params
    params.permit(
      :name,
      :email,
      :password_confirmation,
      :password
    )
  end
end
